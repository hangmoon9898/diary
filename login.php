<html>
  <head>
    <link rel="stylesheet" href="res/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <title>Your own secure digital diary - Lehang Diary</title>
  </head>

  <body>

    <h1>Diary</h1>

    <div id="buttons"><a class="buttonLogin" href="#" onclick="showLogin()">LOGIN</a> <a class="buttonRegister" href="#" onclick="showRegister()">REGISTER</a></div>

    <div id="content">

    </div>

    <script>
      function showLogin() {
        var text ="<div id='login'><h2> LOGIN </h2><form action='res/checkData.php' method='post'><input id='user' name='user' type='text' placeholder='User'><input id='pass' name='pass' type='password' placeholder='Password'><input type='submit' name='submit' value='GO'></form></div>"
        document.getElementById("content").innerHTML = text;
      }

      function showRegister() {
        var text ="<div id='reg'><h2> REGISTER </h2><form action='res/registerData.php' method='post'><input id='user' name='user' type='text' placeholder='User' required><input id='pass' name='pass' type='password' placeholder='Password' required><input id='pass2' name='pass2' type='password' placeholder='Retype Password' required><input type='submit' name='submit' value='Submit'></form></div>"
        document.getElementById("content").innerHTML = text;
      }
    </script>
  </body>
</html>
