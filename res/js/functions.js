function showEditor() {
  var editor = document.getElementById("editor-container");
  editor.style.visibility = "visible";
  var button = document.getElementById("addEntry");
  button.style.visibility = "hidden";

  var quill = new Quill('#editor-container', {
    modules: {
      toolbar: [
        [{ header: [1, 2, false] }],
        ['bold', 'italic', 'underline'],
        ['image', 'code-block']
      ]
    },
    placeholder: 'Compose an epic...',
    theme: 'snow'
  });
}

function showContent() {
  var delta = quill.getContents();
  var length = quill.getLength();
  var packet = JSON.stringify(delta);
  document.getElementById("demo").innerHTML = length;
  document.getElementById("v").value = packet;
}
