<?php

include 'data.php';
include 'func.php';

// Set variables received from the HTML form with the POST method
$user = $_POST["user"];
$pass = $_POST["pass"];

$conn = new mysqli($myHost, $myName, $myPass, $myDaBa);

if ($conn->connect_error) {
    die('Connect Error (' . $conn->connect_errno . ') '
            . $conn->connect_error);
}

// Make SQL query to server, to see if the password given matches the one saved in the DB
$sql = "SELECT `pass`, `id` from `login` WHERE `user`='$user'";
$result = $conn->query($sql);
$row = $result->fetch_assoc();

// Set the variable hashed_password to the value which we received from the DB
$hashed_password = $row["pass"];
$id = $row["id"];
$date = date("y-m-d");

// Display messages for successful & failed (else) login attempts
if (password_verify($pass, $hashed_password) ) {

    // Start a new session to keep the user logged in
    session_start();

    // Set useful Session variables
    $_SESSION["id"] = $id;
    $_SESSION["username"] = strtolower($user);

    header("Location: ../index.php");
} else {
    header("Location: ../login.php");
}

?>
