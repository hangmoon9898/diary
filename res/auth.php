<?php

// start the session
session_start();

// if the user is not logged it --> redirect to login.php
if(!isset($_SESSION["id"])){
  header("Location: login.php");
  exit();
}

?>
