<?php include 'res/functions.php'; ?>
<?php include 'res/auth.php'; ?>

<html>
  <head>
    <link rel="stylesheet" href="res/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Sacramento" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
    <script src="res/js/functions.js"></script>
    <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
  </head>

  <body>
    <div id="userBar">
      <div class="logo"><a href="https://lehang.pw/diary/index.php"> Diary </a></div>
      <div class="avatar">
        <div class="dropdown">
          <button class="dropbtn"><?php echo "<div class='username'>". $_SESSION['username'] . "</div>" . GetAvatar($_SESSION['username']); ?></button>
          <div class="dropdown-content">
            <a href="editprofile.php">Edit profile</a>
            <a href="settings.php">Settings</a>
            <a href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <div class="space"><br></div>

    <a id="addEntry" onclick="showEditor()">+ Entry</a>

    <div id='editor-container'></div>

    <div id="demo">
    </div>

  </body>

  <form action="saveEntry.php" method="post">
    <input type="text" id="v" name="entry" value="" hidden><br><br>
    <div id="saveButton">
      <input type="submit" onclick="showContent()" value="Save">
    </div>
  </form>

  <input type="file" id="paperclip" class="inputfile" data-multiple-caption="{count} files selected" multiple />
  <label for="file">Choose a file</label>

  <script src="res/js/file-upload.js"></script>
</html>
